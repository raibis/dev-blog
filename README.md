# hugo-blog

My Hugo blog

## Start the Hugo server

`hugo server`

Include drafts
`hugo server -D`

## Build static pages

`hugo`

Include drafts
`hugo -D`
