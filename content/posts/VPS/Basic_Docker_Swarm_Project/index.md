---
title: "Docker Swarm project"
date: 2023-04-24T00:00:00+02:00
lastmod: 2023-04-24T00:00:00+02:00
draft: true
authorLink: ""
description: ""

tags: ["Docker", "Swarm"]
categories: ["Docker"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

The project features:

- NodeJs Backend with Mysql
- Vue Js Frontend
- Nginx reverse proxy for backend, frontend build files and Cloudflare certificate
- Wireguard VPN
- Swarmpit console

## OS ad Docker setup

### VPS OS

Using Hostinger VPS with Ubuntu 22.04 64bit.
Login to the server via ssh.

```Bash {linenos=false}
ssh root@x.x.x.x
```

Check kernel version

```Bash {linenos=false}
uname -r
#5.2.0
```

I will use [Wireguard VPN docker image](https://hub.docker.com/r/linuxserver/wireguard). Kernel newer than 5.6 needed. Therefore installing never kernel headers.

```Bash {linenos=false}
sudo apt-get update
sudo apt-get install linux-headers-generic
```

### Docker install

Installing docker as per [documentation](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

[Doing Linux post-install steps for Docker Engine](https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot-with-systemd)

### Docker swarm

Initializing Docker swarm.

```Bash {linenos=false}
docker swarm init
```

### Docker network

Creating Docker network `project`

```Bash {linenos=false}
docker network create --driver overlay --attachable project
```

Checking if the network created

```Bash {linenos=false}
docker network ls
#NETWORK ID     NAME      DRIVER    SCOPE
#da1223f3fda5   bridge    bridge    local
#345061b82018   host      host      local
#gp0dez4e7p9h   ingress   overlay   swarm
#bcf9709c4101   none      null      local
#o9cc3x6g015f   project   overlay   swarm

```

All services will connect to this network

Get network details

```Bash {linenos=false}
docker network inspect project
#...
#     "Subnet": "10.0.1.0/24",
#     "Gateway": "10.0.1.1"
#...
```

## Project services

### Wireguard VPN

Check user id and group id

```Bash {linenos=false}
id
#uid=0(root) gid=0(root) groups=0(root)
```

Creating Wireguard folder where configuratios stored.

```Bash {linenos=false}
sudo mkdir /opt/wireguard/config
#for non root
# sudo chown username:username /opt/wireguard/
```

Creating VPN Docker Compose .yaml file.

```yaml {linenos=false}
version: "3.8"
services:
  wireguard:
    image: linuxserver/wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    environment:
      - PUID=0
      - PGID=0
      - TZ=Europe/Vilnius
      - SERVERURL=auto
      - SERVERPORT=51820
      - PEERS=3
      - PEERDNS=auto
      - INTERNAL_SUBNET=10.0.1.0
      - ALLOWEDIPS=10.0.0.0/8
      - PERSISTENTKEEPALIVE_PEERS=all
    volumes:
      - /opt/wireguard/config:/config
    ports:
      - 51820:51820/udp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    networks:
      - project
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
networks:
  project:
    external: true
```

Create file on server `docker-compose.vpn.yml`

Run service

```Bash {linenos=false}
docker stack deploy --compose-file docker-compose.vpn.yml vpn
```

Check if Wireguard container successfully running in stack `vpn`.

```Bash {linenos=false}
docker stack ps vpn  --no-trunc
```
