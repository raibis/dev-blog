---
title: "Enable Vue Devtools in production"
# subtitle: "Successors academy"
date: 2023-01-17T00:00:00+02:00
lastmod: 2023-01-17T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["Vue", "Devtools"]
categories: ["Vue"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

## Manual

Open Chrome Developer tools Console and run following code.

```javascript {linenos=false}
// Get Vue app instance and version
const app = Array.from(document.querySelectorAll("*")).find(
  (e) => e.__vue_app__
).__vue_app__;
const version = app.version;

// Get Devtools instance
const devtools = window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

// Enable Devtools
devtools.enabled = true;

// Reinitialize Vue app instance
devtools.emit("app:init", app, version, {});
```

Relaod devtools

![Reload devtools](vue_devtools_1.png)

![Reload devtools](vue_devtools_2.png)

Now you can access Vue Devtools

## Extension

Install Crome extension `Vue force dev`

![Vue force dev](vue_devtools_3.png)
