---
title: "Models for hierarchical data"
# subtitle: "Successors academy"
date: 2022-02-19T00:00:00+02:00
lastmod: 2022-02-19T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["DB", "SQL"]
categories: ["DB"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

Tree-like data relationships are common, but working with trees in SQL usually requires awkward recursive queries. This talk describes alternative solutions in SQL, including:

- Adjacency List
- Path Enumeration
- Nested Sets
- Closure Table

{{< youtube wuH5OoPC3hA >}}

[Slides](https://www.slideshare.net/billkarwin/models-for-hierarchical-data)
