---
title: "DBeaver Ubuntu quick update"
# subtitle: "Successors academy"
date: 2023-05-29T00:00:00+02:00
lastmod: 2023-05-29T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["DBeaver"]
categories: ["DB"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

```Bash {linenos=false}
sudo curl -o dbeaver-ce.deb "$(
      curl -sL -I -w '%{url_effective}' \
        https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb  | tail -1 )" && \
sudo apt install ./dbeaver-ce.deb
```
