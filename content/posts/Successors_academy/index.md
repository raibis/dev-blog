---
title: "Successors academy"
# subtitle: "Successors academy"
date: 2022-02-15T00:00:00+02:00
lastmod: 2022-02-15T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["Leadership", "Strategic thinking", "Growth Mindset", "Critical thinking", "Time management", "Priority management", "Receiving feedback", "Giving feedback", "Customer obsession"]
categories: ["Successors academy"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->
## How to be successful?

[Manager of one in GitLab](https://about.gitlab.com/handbook/leadership/)

[20 Growth Mindset Examples to Change Your Beliefs](https://www.developgoodhabits.com/growth-mindset-examples/)

[How our brain can change](https://www.youtube.com/watch?v=ELpfYCZa87g)

[3 simple habits to improve your critical thinking](https://hbr.org/2019/05/3-simple-habits-to-improve-your-critical-thinking)

[How to Be a More Strategic Thinker](https://www.forbes.com/sites/melodywilding/2020/06/01/how-to-be-a-more-strategic-thinker/?sh=724c6d5237e6)

## Time and priorities management


[Time management techniques](https://www.usa.edu/blog/time-management-techniques/)

[The importance of one on one meetings](https://www.cultureamp.com/blog/the-importance-of-1-on-1-meetings)

[5 old-school strategies for prioritising urgent tasks you can try right now](https://monday.com/blog/project-management/5-strategies-for-prioritizing-tasks/?marketing_source=adwordssearch&marketing_campaign=row-s-dsa-e-desk-monday&aw_keyword=&aw_match_type=b&cluster=&subcluster=&gclid=Cj0KCQiAys2MBhDOARIsAFf1D1cP29kgZ6qTPCoVrJnSle5ssOSvk1xWDXs7a_n4m2LC1rp3D2FVJRMaAtNyEALw_wcB)

[How to stop wasting time](https://online.alvernia.edu/articles/guide-to-effective-meetings/)

[The Silent Meeting Manifesto v1: Making meeting suck a little less](https://medium.com/swlh/the-silent-meeting-manifesto-v1-189e9e3487eb)

## Customer obsession

[How to Make Customer Obsession a Company Habit](https://www.userlike.com/en/blog/customer-obsession)

[7 habits of Customer Obsessed companies](https://www.linkedin.com/pulse/7-habits-customer-obsessed-companies-mohanbir-sawhney/)

## Giving and receiving feedback

[The Importance Of The Employee Feedback Loop](https://www.forbes.com/sites/forbescommunicationscouncil/2018/08/01/the-importance-of-the-employee-feedback-loop/?sh=5c02eb202026)

[How to Get the Feedback You Need](https://hbr.org/2015/05/how-to-get-the-feedback-you-need)

[Being a great listener can benefit your career](https://www.fastcompany.com/90421293/being-a-great-listener-can-benefit-your-career-heres-how-to-do-it)

[The Situation-Behavior-Impact-Feedback Framework](https://medium.com/pm101/the-situation-behavior-impact-feedback-framework-e20ce52c9357)

## How to be a good DRI?

[ Directly Responsible Individuals: The What, How and Why of DRIs](https://tettra.com/article/directly-responsible-individuals-guide/)

[Titanic - Project Management Blunders - a complex project management example](https://www.youtube.com/watch?v=wbvfir2x344)

## Principles

[Ray Dalio principles ](https://www.youtube.com/watch?v=B9XGUpQZY38)

[Why Core Values Matter](https://www.forbes.com/sites/brentgleeson/2021/03/30/why-core-values-matter-and-how-to-get-your-team-excited-about-them/?sh=7c3a2ae34afd)
