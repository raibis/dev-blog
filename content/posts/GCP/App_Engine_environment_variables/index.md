---
title: "App Engine environment variables"
# subtitle: "Successors academy"
date: 2022-04-19T00:00:00+02:00
lastmod: 2022-04-19T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["GCP", "gcloud", "App Engine"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

Load environment variables.

<!--more-->

## Nonsecret environment variables

You can add no secret environment variables simply by adding a couple of lines in `app.yaml` configuration file.

```yaml
runtime: nodejs14
env_variables:
  NOT_SECRET_VARIABLE: "NOT_SECRET_VARIABLE"
```

Node.Js aplication can get `NOT_SECRET_VARIABLE` value from `process.env.NOT_SECRET_VARIABLE`.

DO NOT use such a method for secret environment variables, because the `app.yaml` values are visible in the repo. So everyone having access to the repo can see secrets.

## Secret environment variables

Mandatory method for securely storing usernames, passwords, tokens, keys, and other secure information.

### Enable the Secret Manager API

[Go to Secret Manager API](https://console.cloud.google.com/apis/library/secretmanager.googleapis.com)

![Secret Manager API](001_environment_variables.png)

### Add permissions for application access Secret Manager API

![Secret Manager API](002_environment_variables.png)  
Select edit `App Engine default service account`
![Secret Manager API](003_environment_variables.png)  
Click Add another role. Select a role to add, such as Secret Manager Secret Accessor
![Secret Manager API](004_environment_variables.png)

### Add secret  

![Secret Manager API](005_environment_variables.png)  
![Secret Manager API](006_environment_variables.png)  
![Secret Manager API](007_environment_variables.png)  
![Secret Manager API](008_environment_variables.png)  

### Write code  

`server.js`  
```javascript
const express = require('express');
const {SecretManagerServiceClient} = require('@google-cloud/secret-manager');
const client = new SecretManagerServiceClient();

async function getSecretValue(secret, v = "latest") {
  const [version] = await client.accessSecretVersion({
    name: `projects/${process.env.GOOGLE_CLOUD_PROJECT}/secrets/${secret}/versions/${v}`,
  });
  const payload = version.payload.data.toString('utf8');
  return payload;
}

const app = express();

app.get('/', async (req, res) => {
  const secretValue = await getSecretValue(process.env.SECRET_VARIABE);
  res.send(`Hello from App Engine! I see ${process.env.NOT_SECRET_VARIABLE} env variable! I see secret value ${secretValue}`);
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
```  
Updating as well `app.yaml`  
```yaml
runtime: nodejs14
env_variables:
  NOT_SECRET_VARIABLE: "NOT_SECRET_VARIABLE"
  SECRET_VARIABE: "secret_value"
```

