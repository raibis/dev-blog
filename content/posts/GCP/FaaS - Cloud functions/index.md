---
title: "FaaS - Cloud functions"
# subtitle: "Successors academy"
date: 2022-10-20T00:00:00+02:00
lastmod: 2022-10-20T00:00:00+02:00
draft: true
authorLink: ""
description: ""

tags: ["GCP", "Cloud functions", "FaaS"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

I explore Google cloud functions using VS Code Cloud Code extension

<!--more-->

Install VS Code extension Cloud Code. You might need as well install [Google Cloud CLI](/posts/gcp/installing_google_cloud_cli/)

Select Cloud Function API under Cloud Apis section

![Cloud Function API](001_cloud_function.png)

Create new project

![Create new project](002_cloud_function.png)

Enable Cloud Function API

![Enable Cloud Function API](003_cloud_function.png)

Enable Secret Manager API If you will use secure environment variables

![Enabling Secret Manager API](008_cloud_function.png)

Select secret manager project

![Select secret manager project](010_cloud_function.png)

Create secret

![Create secret](009_cloud_function.png)

![Create secret](011_cloud_function.png)

Create Function

![Create Cloud Function](004_cloud_function.png)

You might need enable missing APIs

![Enable APIs](005_cloud_function.png)

Configure Basic part. Selecting 2nd generation Cloud Functions, giving a name for this cloud function, and selecting the closest region. Allow public access for testing purposes.
It's always a good practice to set an authorization on your cloud functions. Therefore on default Cloud Functions require authorization.

![Filling in Basic settings](006_cloud_function.png)

Expanding `Runtime, build, connections and security settings`.
Changing Runtime settings as per requirements. Adding nonsecure environment variable.

![Filling in Runtime settings](007_cloud_function.png)

Reference secret environment variable

![Reference secret environment variable](012_cloud_function.png)

![Reference secret environment variable](013_cloud_function.png)

Might need enable more APIs if using 2nd gen Cloud Functions

![Enable APIs](014_cloud_function.png)

Go to next Code step

![Enter code](015_cloud_function.png)

Let's deploy the default code. Wait a bit until deployment finishes. If build fails you might need to edit function details and grant permissions for secret access.

![Successful deployment](016_cloud_function.png)
