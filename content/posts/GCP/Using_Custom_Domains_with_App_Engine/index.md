---
title: "Using Custom Domains with App Engine"
# subtitle: "Successors academy"
date: 2022-04-18T00:00:00+02:00
lastmod: 2022-04-18T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["GCP", "App Engine", "Domain"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

I explore using custom domains with App Engine. 

<!--more-->

App Engine aplication [Creating Google App Engine Project](/posts/gcp/creating_google_app_engine_project/) has google asigned `appspot.com` subdomain. For example `https://argon-retina-346404.uc.r.appspot.com`.

I want to assign the custom domain `testapp.raibis.lt` for this application.

Visit App Engine > Settings > Custom Domains > Add Custom Domain. 

![Custom Domain App Engine](001_custom_domain.png)

Click Add Custom Domain

![Webmaster Central](002_custom_domain.png)

Clicking Verify will redirect to Google Webmaster Central. Here have to select the Other option. 

![Webmaster Central](003_custom_domain.png)

I have to visit Cloudflare and add two DNS records: CNAME `testapp` for subdomain`testapp.raibis.lt` and TXT record for site verification.

![Coudflare add CNAME](004_custom_domain.png)

![Cloudflare add TXT](005_custom_domain.png)

Come back to Webmaster Central and press Verify button.

![Successfull domain merification](006_custom_domain.png)

Go back to Google Console and click Refresh Domain. Now you should see it like this.

![Custom Domain App Engine](007_custom_domain.png)

![Custom Domain App Engine](008_custom_domain.png)

![Custom Domain App Engine](009_custom_domain.png)

Now I have to come back to Cloudflare and add the listed above A and AAAA DNS records for `testapp.raibis.lt`.
You have to delete CNAME first, before adding A and AAAA records.

![Custom Domain App Engine](010_custom_domain.png)

It can take up to 24h while updated A and AAAA propagate.
I can check domain propogation status with `dnschecker.org` [testapp.raibis.lt DNS status](https://dnschecker.org/#A/testapp.raibis.lt).

Google certificate generation will fail.

![Custom Domain App Engine](011_custom_domain.png)

I am using Cloudflare strict encryption mode.

![Custom Domain App Engine](012_custom_domain.png)

Therefore I have to generate an Origin certificate and add it to App Engine. In Cloudflare raibis.lt > SSL/TLS > Origin Server

![Custom Domain App Engine](013_custom_domain.png)

Select RSA private key type. Add `testapp.raibis.lt` to host names.

![Custom Domain App Engine](014_custom_domain.png)

Clicking Create will create certificates. I have to upload certificate to App Engine.

![Coudflare Origin certificate](015_custom_domain.png)

Upload a new certificate.

![Coudflare Origin certificate](016_custom_domain.png)

Copy/paste the Public key certificate and Private key. Here starts the fun part. You will get a Private key invalid error.

![Coudflare Origin certificate](017_custom_domain.png)

Fortunately this is easy to fix by renaming private key parts `-----BEGIN PRIVATE KEY-----` and `-----END PRIVATE KEY-----` 
to `-----BEGIN RSA PRIVATE KEY-----` and `-----END RSA PRIVATE KEY-----` 

![Coudflare Origin certificate](018_custom_domain.png)

Upload and enable certificate.

![Coudflare Origin certificate](019_custom_domain.png)

Now you can visit `testapp.raibis.lt`. Be patient you might get `525` error. In this case, you might need to wait sometime.

![Coudflare Certificate Error](020_custom_domain.png)

While waiting you can check if your SSL Certificate is valid. 
To view the status of your SSL certificate, you can use an [SSL certificate checker tool](https://www.ssllabs.com/ssltest/analyze.html)   
![Coudflare Certificate Error](021_custom_domain.png)

Congratulation now you can visit [App engine app directly](https://argon-retina-346404.uc.r.appspot.com)
 and from [testapp.raibis.lt](https://testapp.raibis.lt)