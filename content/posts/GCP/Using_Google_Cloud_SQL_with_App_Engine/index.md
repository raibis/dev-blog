---
title: "Using Google Cloud SQL with App Engine"
# subtitle: "Successors academy"
date: 2022-04-20T00:00:00+02:00
lastmod: 2022-04-20T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["GCP", "App Engine", "Cloud SQL", "DBeaver"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

I explore using Google Cloud SQL.

<!--more-->

## Create DB

Visit SQL section.

![Cloud SQL](001_sql.png)

Select MYSQL.

![Cloud SQL](002_sql.png)

Enabling Compute Engine API.

![Cloud SQL](003_sql.png)

I selecting MySQL 8.0 region same as App Engine region. This is test DB, thefore setting lowest instance resources.

![Cloud SQL](004_sql.png)

New project MySQL instance created.

![Cloud SQL](005_sql.png)

Creating `test_database` database

![Cloud SQL](006_sql.png)

## Connect to the DB

Connect to the db via DBeaver.
By default, all external IP addresses are blocked from reaching your Cloud SQL instance.

[Find out your IP address](https://www.google.com/search?q=my+ip)

Add your IP to Authorized networks.

![Cloud SQL](007_sql.png)

Let's test DB connection.

![Cloud SQL](008_sql.png)

By default, Cloud SQL allows unencrypted connections from authorized networks. This is not good, as it allows traffic to be intercepted, possibly leading to a data breach.

Recommend you use the Cloud SQL SQL proxy to connect DBeaver to Cloud SQL.

Install SQL proxy on linux 64bit:

```bash
wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
chmod +x cloud_sql_proxy
```

In order to run the proxy we need to get the instance name from the developer console.  [Go to the Cloud SQL instance page](https://console.cloud.google.com/sql/instances) and click the instance name to open its Instance details page.

![Cloud SQL](010_sql.png)

Start the proxy in its own terminal so you can monitor its output. Replace <INSTANCE_CONNECTION_NAME> with the instance connection name you copied in the previous step. Replace <PORT>.

```bash
./cloud_sql_proxy -instances=<INSTANCE_CONNECTION_NAME>=tcp:<PORT>
```

![Cloud SQL](009_sql.png)

Testm Cloud SQL proxy connection.

![Cloud SQL](011_sql.png)

Then you can click the Test Connection button and you should see.

![Cloud SQL](012_sql.png)

Conncection via Cloud SQL Proxy successfull. 
Now I can enable `Allow only SSL connections` and restart service.

![Cloud SQL](013_sql.png)

As expected, unsecure conection is failing, althougt connection via proxy work fine.

![Cloud SQL](014_sql.png)

