---
title: "Installing Google Cloud CLI on Ubuntu 20.04.4 LTS"
# subtitle: "Successors academy"
date: 2022-04-10T00:00:00+02:00
lastmod: 2022-04-10T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["GCP", "gcloud", "Ubuntu"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

The Google Cloud CLI `gcloud` manages authentication, local configuration, developer workflow, and general interactions with Google Cloud resources. With the Google Cloud CLI, it’s easy to perform many common cloud tasks like creating a Compute Engine VM instance, managing a Google Kubernetes Engine cluster, and deploying an App Engine application, either from the command line or in scripts and other automations.

<!--more-->

I am running Ubuntu 20.04.4 LTS and want `gcloud` and `gsutil` to automatically update to the latest version. Therefore I will use Snap Package Manager for installation.

At the command line, install the gcloud CLI snap package:

```shell
snap install google-cloud-cli --classic
```

Initialize the gcloud CLI and get started

```shell
gcloud init
```

Follow instructions and login into your account.
