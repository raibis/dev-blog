---
title: "Creating Google App Engine Project"
# subtitle: "Successors academy"
date: 2022-04-12T00:00:00+02:00
lastmod: 2022-04-12T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["GCP", "gcloud", "App Engine"]
categories: ["GCP"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

This quickstart shows how to deploy a sample app on App Engines.

<!--more-->

## Before you begin

1. In the Google Cloud Console, on the project selector page, select or create a Google Cloud project. [Go to project selector](https://console.cloud.google.com/projectselector2/home/dashboard)

2. Make sure that billing is enabled for your Cloud project. Learn how to check if billing is enabled on a project. Check Billing health checks.

![Billing health checks](001_billing_health_check.png)

3. Enable the Cloud Build API. [Go to Cloud Build API](https://console.cloud.google.com/apis/library/cloudbuild.googleapis.com)

![Cloud Build API](002_cloud_build_api.png)

4.Install and initialize the Google Cloud CLI. [Installing Google Cloud CLI on Ubuntu 20.04.4 LTS](/posts/gcp/installing_google_cloud_cli/)

5. Create an App Engine application for your Cloud project in the Google Cloud Console. [Open app creation](https://console.cloud.google.com/appengine/create?lang=nodejs&st=true)

6. Select a region where you want your app's computing resources located.

## Write basic web service with Node.js

[GitLab repo](https://gitlab.com/raibis/app-engine-test)

server.js

```javascript {title="main.go"}
const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("Hello from App Engine!");
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
```

package.json

```json
{
  "name": "app-engine-test",
  "version": "1.0.0",
  "description": "App Engine Test",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "express": "^4.17.3"
  }
}
```

app.yaml

```yaml
runtime: nodejs14
```

## Deploy your service on App Engine

In your Node.js service folder, where the app.yaml file is located, run the following command in your terminal:

```shell
gcloud app deploy
```

If all ok you should see something similar

![Deploy App Engine](003_deploy.png)

The `target url` is app location. You can use as well the following command:

```shell
gcloud app browse
```

[You can access web service here](https://argon-retina-346404.uc.r.appspot.com)
