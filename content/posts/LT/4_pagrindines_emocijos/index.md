---
title: "4 pagrindinės emocijos"
# subtitle: "Successors academy"
date: 2022-01-01T00:00:00+02:00
lastmod: 2022-01-01T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["Ψ"]
categories: ["LT"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

![Keturios emocijos legeda](4_emocijos_legenda.svg)

## Laimingas

![Keturios emocijos - Laimingas](4_emocijos_laimingas.svg)

> ### Turiu tai, ko noriu / ko man reikia.

| Panašūs jausmai ir skirtingi jų lygiai                                                                                             |
| ---------------------------------------------------------------------------------------------------------------------------------- |
| Patenkintas, saugus, pasitikintis, ramus, priimtas, džiaugsmingas, smalsus, taikus, atsipalaidavęs, dėkingas, mylimas, įsitraukęs. |

| Ką daryti? |
| ---------- |
| Mėgautis!  |

---

## Piktas

![Keturios emocijos - Piktas](4_emocijos_piktas.svg)

> ### Kažkas trukdo man pasiekti tai, ko noriu / ko man reikia. Arba kažkas peržengia mano ribą.

| Panašūs jausmai ir skirtingi jų lygiai                                                          |
| ----------------------------------------------------------------------------------------------- |
| Susierzinęs, nekantrus, sudirgęs, sumišęs, jaučiantis apmaudą, pasipiktinęs, įsižeidęs, įsiutęs |

| Klausimai sau            |
| ------------------------ |
| Ko aš noriu?             |
| Ko man reikia?           |
| Kas man svarbu?          |
| Kas tam trukdo?          |
| Kas peržengia mano ribą? |

| Ką daryti?                                                       |
| ---------------------------------------------------------------- |
| Suprasti poreikį ar norą bei suprasti, kas neleidžia to gauti.   |
| Suprasti, kas ir kokią ribą peržengia.                           |
| Paprašyti / pasiimti tai, ko noriu.                              |
| Paprašyti, kad kitas neperžengtų ribos arba apsaugoti savo ribą. |

| Pavyzdžiai                                    |
| --------------------------------------------- |
| Kitas vėluoja.                                |
| Kitas mane pertraukia.                        |
| Neišgirsta to, ką sakau.                      |
| Noriu dėmesio iš draugo/draugės.              |
| Kitas man duoda patarimą, kurio man nereikia. |

| Reikalingi veiksmai                                                                                                                                                  |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Pasakyti, kaip jaučiuosi, kad kitas vėluoja bei paprašyti nevėluoti.                                                                                                 |
| Paprašyti, kad nepertrauktų ir leistų man užbaigti.                                                                                                                  |
| Paprašyti, kad labiau įsiklausytų į mane ir dar aiškiau pasakyti tai, kas man svarbu.                                                                                |
| Pakviesti draugą / draugę susitikti / kalbėti.                                                                                                                       |
| Sustabdyti, jei kažkas duoda patarimą, kurio man nereikia, ir, pvz., paprašyti, kad kitas atspindėtų tai, ką girdi, pasidalintų savo patirtimi panašiose situacijose |

## Liūdnas

![Keturios emocijos - Liūdnas](4_emocijos_liudnas.svg)

> ### Aš praradau kažką, ko man reikėjo / kas man buvo svarbu

| Panašūs jausmai ir skirtingi jų lygiai                                                          |
| ----------------------------------------------------------------------------------------------- |
| Užsidaręs, nutolęs, prislėgtas, nusiminęs, vienišas, jaučiantis širdgėlą, gedintis, nelaimingas |

| Klausimai sau |
| ------------- |
| Ko netekau?   |

| Ką daryti?                                                         |
| ------------------------------------------------------------------ |
| Suvokti, ko netekau.                                               |
| Pripažinti netektį.                                                |
| Skirti laiko atsisveikinti su tuo, kas buvo ir ko nebėra, išgedėti |

| Pavyzdžiai                              |
| --------------------------------------- |
| Išsiskyriau su brangiu žmogumi.         |
| Netekau darbo.                          |
| Netekau mėgstamos veiklos.              |
| Patyriau negrįžtamą sveikatos problemą. |

| Reikalingi veiksmai                                 |
| --------------------------------------------------- |
| Pripažinti, kad netekau brangaus žmogaus.           |
| Paprašyti laiko pasidalinti, kaip dėl to jaučiuosi. |
| Išsakyti jam tai, kas liko neišsakyta.              |
| Leisti sau liūdėti, verkti.                         |
| Paprašyti palaikymo.                                |

## Išsigandęs

![Keturios emocijos - Išsigandęs](4_emocijos_issigandes.svg)

> ### Aš galiu prarasti save arba tai, ką turiu.

| Panašūs jausmai ir skirtingi jų lygiai                                  |
| ----------------------------------------------------------------------- |
| Susirūpinęs, sunerimęs, nesaugus, pažeidžiamas, bijantis, panikuojantis |

| Klausimai sau               |
| --------------------------- |
| Ką man svarbu išsiaiškinti? |
| Ką aš noriu išsiaiškinti?   |

| Ką daryti?                                     |
| ---------------------------------------------- |
| Surinkti reikiamus duomenis.                   |
| Veikti.                                        |
| Pereiti per baimę ir perėjus atsigręžti atgal. |
| Arba nuspręsti neiti ten, kur baisu.           |

| Pavyzdžiai                                                                                             |
| ------------------------------------------------------------------------------------------------------ |
| Baisu kalbėti kažkokia tema, nes galiu būti nepriimtas, kitiems bus nuobodu, bijau būti sukritikuotas. |
| Baisu konfrontuoti, nenoriu konflikto.                                                                 |
| Baisu keisti gyvenimo veiklą.                                                                          |
| Baisu prisipažinti.                                                                                    |

| Reikalingi veiksmai                                                                                            |
| -------------------------------------------------------------------------------------------------------------- |
| Rinktis kalbėti ta tema arba pasidalinti, kad noriu kalbėti ir tuo pačiu baisu.                                |
| Konfrontuoti nepaisant to, kad baisu arba pasidalinti, kad noriu konfrontuoti ir tuo pačiu nepatogu tą daryti. |
| Išeiti iš darbo ir ieškoti naujo, nepaisant to, kad baisu.                                                     |
| Arba nuspręsti nekeisti darbo, nes saugumas šiuo metu svarbiau.                                                |
