---
title: "Visual Studio Code Settings"
# subtitle: "Successors academy"
date: 2023-10-24T00:00:00+02:00
lastmod: 2023-10-24T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["Visual Studio Code"]
categories: ["VSCode"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

<!--more-->

Edit Visual Studio Code users `settings.json` settings by pressing `Ctrl + Shift + p` and selecting `Preferences: Open User Settings (JSON)`.

## Bracket Colors

```JSON
  "editor.guides.bracketPairs": "active",
  "editor.guides.bracketPairsHorizontal": true,
  "editor.bracketPairColorization.enabled": true,
  "workbench.colorCustomizations": {
    "editorIndentGuide.background1": "#666666"
  },
```

## Color Theme

`Community Material Theme High Contrast`

## File Icon Theme

`Material Icon Theme`
