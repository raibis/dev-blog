---
title: "Ubuntu: Show your branch name on your terminal"
# subtitle: "Successors academy"
date: 2022-05-29T00:00:00+02:00
lastmod: 2022-05-29T00:00:00+02:00
draft: false
authorLink: ""
description: ""

tags: ["Ubuntu","git", "terminal"]
categories: ["Ubuntu"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: false
math:
  enable: false
lightgallery: false
license: ""
---

Show your branch name on your terminal.

<!--more-->

Add these lines in your `~/.bashrc` file.

```shell
# Show git branch name
force_color_prompt=yes
color_prompt=yes
parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
if [ "$color_prompt" = yes ]; then
 PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(parse_git_branch)\[\033[00m\]\$ '
else
 PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w$(parse_git_branch)\$ '
fi
unset color_prompt force_color_prompt
```
Reload the `.bashrc` file with this command:

```shell
source ~/.bashrc
```
